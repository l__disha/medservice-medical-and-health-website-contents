import $ from 'jquery';
class Modal {
    constructor() {
        this.openModalButtons = $(".open-modal");
        this.modal = $(".modal");
        this.modalCloseButton = $(".modal-close");
        this.events();
    }
    events() {
        this.openModalButtons.click(this.openModal.bind(this)) // ()=> "arrow operator" automatically bind karta hai, as yeh bahot late aaya tha pehle bind() tha jiske argument mai hum pass karte hai ki kisko bind karna hai. (only used to bind "this" keyword thou).
        this.modalCloseButton.click(this.closeModal.bind(this));
        $(document).keyup(this.keyPressHandler.bind(this));
    }

    openModal(){
        this.modal.addClass("modal-is-visible");
        return false; // iss se preventDefault() hua kaise? if callback function returns false then it doesn't perform the original task which the event intends to do.
    }
    closeModal() {
        this.modal.removeClass("modal-is-visible");
    }
    keyPressHandler(e) {
        if(e.keyCode == 27){
            this.closeModal();
        }
    }
}

export default Modal;