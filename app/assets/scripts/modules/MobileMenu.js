// 1. Select The element
// 2. Add Listener to it
// 3. Write a fucntion which performsjob when event occurs
// document.querySelector("selector").addEventListener("click", function() {

// });

class MobileMenu {
    constructor() {
        // Select The Element
        this.menuIcon = document.querySelector(".mobile-header-icon");
        this.mobileHeader = document.querySelector(".mobile-menu");
        this.events();
    }

    events() {
        // this.menuIcon.addEventListener("click", this.toggleMenu()); // yeh nai chalega kyuki this idhar will point to menuIcon kyuki yeh uska click function ko call kar raha hai.
        this.menuIcon.addEventListener("click", ()=>this.toggleMenu()); // idhar aata hai error operator js ka, this will prevent 
    }
    toggleMenu() {
        this.mobileHeader.classList.toggle("mobile-menu-active");
        this.menuIcon.classList.toggle("mobile-header-icon-close");
    }
}

export default MobileMenu;